<?php

namespace App\Controllers;

use Wasf\Routing\BaseController;

class HomeController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth')->only('dashboard');
    }

    public function index()
    {
        return __METHOD__;
    }
}
