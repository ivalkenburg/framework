<?php

namespace App\Middleware;

use Wasf\Routing\Middleware\BaseMiddleware;
use Wasf\Routing\Request;

class AnotherTest extends BaseMiddleware
{
    protected function process(Request $request)
    {
        if ($request->test == 'yes') {
            return 'another_middleware_test';
        }
    }
}
