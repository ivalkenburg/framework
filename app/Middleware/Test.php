<?php

namespace App\Middleware;

use Wasf\Routing\Middleware\BaseMiddleware;
use Wasf\Routing\Request;

class Test extends BaseMiddleware
{
    protected function process(Request $request)
    {
        return 'test_middleware';
    }
}
