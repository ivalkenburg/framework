<?php

namespace App\Middleware;

use Wasf\Routing\Middleware\TrimInput as BaseTrimInput;

class TrimInput extends BaseTrimInput
{
    /**
     * @var array Fields that are exempt from trim.
     */
    protected $except = [];
}
