<?php

namespace App;

use Wasf\Core\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    /**
     * @var array References to middleware to be executed before destination and after response.
     */
    protected $middleware = [
        \Wasf\Routing\Middleware\CheckMaintenance::class,
        \App\Middleware\TrimInput::class,
        \Wasf\Routing\Middleware\NullEmptyString::class,
        \Wasf\Routing\Middleware\Session::class,
    ];

    /**
     * @var array Aliases of middleware for referencing in controller and routes.
     */
    protected $middlewareAliases = [
        'auth'         => \Wasf\Routing\Middleware\Auth::class,
        'test'         => \App\Middleware\Test::class,
        'another_test' => \App\Middleware\AnotherTest::class,
    ];
}
