<?php

require_once __DIR__.'/../vendor/autoload.php';

use Wasf\Apps\Kernel;
use Wasf\Routing\Request;

Kernel::process(app(Request::class, $_SERVER, $_GET, $_POST, $_COOKIE));
