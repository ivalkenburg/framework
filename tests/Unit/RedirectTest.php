<?php

namespace Tests\Unit;

use Tests\TestCase;
use Wasf\Apps\Redirect;
use Wasf\Apps\Router;
use Wasf\Routing\Response\RedirectResponse;

class RedirectTest extends TestCase
{
    /** @test */
    public function factory_can_create_url_redirect()
    {
        $redirect = Redirect::to('http://test.com');

        $this->assertInstanceOf(RedirectResponse::class, $redirect);
        $this->assertEquals('http://test.com', $redirect->getHeaders()['Location']);
        $this->assertContains('<!doctype html>', (string) $redirect);

        $redirect = redirect('/');

        $this->assertInstanceOf(RedirectResponse::class, $redirect);
        $this->assertEquals('/', $redirect->getHeaders()['Location']);
        $this->assertContains('<!doctype html>', (string) $redirect);
    }

    /** @test */
    public function factory_can_create_redirect_with_route_name()
    {
        Router::flush();
        Router::get('/home', function () {
            return response('foobar');
        })->name('home');

        $redirect = redirect()->route('home');

        $this->assertInstanceOf(RedirectResponse::class, $redirect);
        $this->assertEquals('/home', $redirect->getHeaders()['Location']);
        $this->assertContains('<!doctype html>', (string) $redirect);

        $redirect = Redirect::route('home');

        $this->assertInstanceOf(RedirectResponse::class, $redirect);
        $this->assertEquals('/home', $redirect->getHeaders()['Location']);
        $this->assertContains('<!doctype html>', (string) $redirect);
    }
}
