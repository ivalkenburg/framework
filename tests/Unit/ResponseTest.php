<?php

namespace Tests\Unit;

use Tests\TestCase;
use Wasf\Apps\Response;
use Wasf\Apps\View;
use Wasf\Routing\Response\ViewResponse;

class ResponseTest extends TestCase
{
    /** @test */
    public function can_return_a_view_response()
    {
        View::share('share', 'foobar');

        $headers = [
            'Content-Type' => 'text/html',
            'Foo'          => 'bar',
        ];

        $response = Response::view('test', ['foo' => 'bar'])->withHeaders($headers);

        $this->assertInstanceOf(ViewResponse::class, $response);
        $this->assertNull($response->getStatus());
        $this->assertEquals($headers, $response->getHeaders()->all());
        $this->assertContains('Foo: bar', (string) $response);
        $this->assertContains('Share: foobar', (string) $response);

        $response = response()->view('test', ['foo' => 'bar'], 404)->header('Content-Type', 'text/html');

        $this->assertInstanceOf(ViewResponse::class, $response);
        $this->assertEquals(404, $response->getStatus());
        $this->assertEquals('text/html', $response->getHeaders()['Content-Type']);
        $this->assertContains('Foo: bar', (string) $response);
        $this->assertContains('Share: foobar', (string) $response);

        $response = view('test', ['foo' => 'bar'])->with('test0r', 'yup');

        $this->assertInstanceOf(ViewResponse::class, $response);
        $this->assertNull($response->getStatus());
        $this->assertContains('Foo: bar', (string) $response);
        $this->assertContains('Share: foobar', (string) $response);
        $this->assertContains('test0r: yup', (string) $response);
    }

    /** @test */
    public function can_return_a_json_response()
    {
        $response = response(['foo' => 'bar'], 404);

        $this->assertEquals('application/json', $response->getHeaders()['Content-Type']);
        $this->assertEquals(404, $response->getStatus());
        $this->assertJson((string) $response);

        $response = response()->json(['foo' => 'bar'], 500)->header('Foo', 'bar');

        $this->assertEquals('application/json', $response->getHeaders()['Content-Type']);
        $this->assertEquals('bar', $response->getHeaders()['Foo']);
        $this->assertEquals(500, $response->getStatus());
        $this->assertJson((string) $response);
    }
}
