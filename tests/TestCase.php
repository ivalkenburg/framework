<?php

namespace Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;
use Wasf\Apps\Kernel;
use Wasf\Apps\Router;
use Wasf\Core\ServiceContainer;
use Wasf\Routing\Request;

class TestCase extends BaseTestCase
{
    public function setUp()
    {
        Router::flush();
        ServiceContainer::flush();
    }

    protected function get($url, $get = [], $headers = [], $cookies = [])
    {
        return Kernel::process(
            $this->buildRequest($url, 'GET', $get, [], $headers, $cookies)
        );
    }

    protected function post($url, $post = [], $headers = [], $cookies = [])
    {
        return Kernel::process(
            $this->buildRequest($url, 'POST', [], $post, $headers, $cookies)
        );
    }

    protected function buildRequest($url, $method, $get = [], $post = [], $headers = [], $cookies = [])
    {
        $default = [
            'HTTP_UPGRADE_INSECURE_REQUESTS' => '1',
            'HTTP_ACCEPT_ENCODING'           => 'gzip, deflate, br',
            'HTTP_ACCEPT_LANGUAGE'           => 'en-US,en;q=0.5',
            'HTTP_ACCEPT'                    => 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'HTTP_USER_AGENT'                => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0',
            'HTTP_HOST'                      => 'framework.dev',
            'REQUEST_URI'                    => $url,
            'REQUEST_METHOD'                 => strtoupper($method),
        ];

        $server = array_merge($default, $headers);

        return app()->forget(Request::class)->get(Request::class, $server, $get, $post, $cookies);
    }
}
