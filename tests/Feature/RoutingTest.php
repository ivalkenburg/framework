<?php

namespace Tests\Feature;

use Tests\TestCase;
use Wasf\Apps\Router;
use Wasf\Routing\Request;
use Wasf\Routing\Response\ExceptionResponse;
use Wasf\Routing\Response\RedirectResponse;
use Wasf\Routing\Response\Response;

class RoutingTest extends TestCase
{
    /** @test */
    public function can_get_to_a_callback()
    {
        Router::get('/test', function () {
            return response('foobar');
        });

        $response = $this->get('/test');

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('foobar', (string) $response);
    }

    /** @test */
    public function can_post_to_a_callback()
    {
        Router::post('/test', function () {
            return response('foobar');
        });

        $response = $this->post('/test');

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('foobar', (string) $response);
    }

    /** @test */
    public function receive_a_404_response_on_non_existent_route()
    {
        Router::post('/foobar', function () {
            return response('foobar');
        });

        $response = $this->get('/foobar');

        $this->assertInstanceOf(ExceptionResponse::class, $response);
        $this->assertEquals(404, $response->getStatus());
    }

    /** @test */
    public function get_can_accept_arguments()
    {
        Router::get('/user/{user}/action/{action}', function ($user, $action) {
            return response($user.' loves '.$action);
        });

        $response = $this->get('/user/foobar/action/testing');

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('foobar loves testing', (string) $response);
    }

    /** @test */
    public function post_can_accept_arguments()
    {
        Router::post('/user/{user}/action/{action}', function ($user, $action) {
            return response($user.' loves '.$action);
        });

        $response = $this->post('/user/foobar/action/testing');

        $this->assertInstanceOf(Response::class, $response);
        $this->assertContains('foobar loves testing', (string) $response);
    }

    /** @test */
    public function post_can_receive_data()
    {
        Router::post('/post', function (Request $request) {
            return response($request->foo.' + '.$request->bar);
        });

        $response = $this->post('/post', ['foo' => 'bar', 'bar' => 'baz']);

        $this->assertContains('bar + baz', (string) $response);
    }

    /** @test */
    public function can_put_constraints_on_get_route()
    {
        Router::get('/foo/{bar}', function ($bar) {
            return response('bar = '.$bar);
        })->where('bar', '\d+');

        $response = $this->get('/foo/4242');

        $this->assertContains('bar = 4242', (string) $response);

        $response = $this->get('/foo/bar');

        $this->assertInstanceOf(ExceptionResponse::class, $response);
        $this->assertContains('404 Not Found', (string) $response);
    }

    /** @test */
    public function can_put_constraints_on_post_route()
    {
        Router::post('/foo/{bar}', function ($bar) {
            return response('bar = '.$bar);
        })->where('bar', 'id-[\d]+');

        $response = $this->post('/foo/id-12345');

        $this->assertContains('bar = id-12345', (string) $response);

        $response = $this->post('/foo/id-12345abcdefg');

        $this->assertInstanceOf(ExceptionResponse::class, $response);
        $this->assertContains('404 Not Found', (string) $response);
    }

    /** @test */
    public function get_can_receive_query_data()
    {
        Router::get('/get', function (Request $request) {
            return response($request->foo.' + '.$request->bar);
        });

        $response = $this->get('/get', ['foo' => 'bar', 'bar' => 'baz']);

        $this->assertContains('bar + baz', (string) $response);
    }

    /** @test */
    public function can_receive_redirect_to_named_route()
    {
        Router::get('/redirect', function () {
            return redirect()->route('home');
        });
        Router::get('/', function () {
        })->name('home');

        $response = $this->get('/redirect');

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('/', $response->getHeaders()->get('Location'));
    }

    /** @test */
    public function can_receive_redirect_back()
    {
        Router::get('/back', function () {
            return back();
        });

        $response = $this->get('/back', [], ['HTTP_REFERER' => '/test']);

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('/test', $response->getHeaders()->get('Location'));
    }

    /** @test */
    public function can_receive_fallback_on_redirect_back()
    {
        Router::get('/fail', function () {
            return back('/ohno');
        });

        $response = $this->get('/fail');

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals('/ohno', $response->getHeaders()->get('Location'));
    }

    /** @test */
    public function can_input_get_trimmed_of_whitespaces()
    {
        Router::post('/input', function (Request $request) {
            return response("'{$request->foo}' '{$request->bar}'");
        });

        $response = $this->post('/input', ['foo' => '     bar     ', 'bar' => 'baz']);

        $this->assertContains("'bar' 'baz'", (string) $response);
    }

    /** @test */
    public function can_input_get_nulled_when_empty()
    {
        Router::post('/input', function (Request $request) {
            return response(gettype($request->foo));
        });

        $response = $this->post('/input', ['foo' => '']);

        $this->assertContains('NULL', (string) $response);
    }

    /** @test */
    public function middleware_can_be_bound_to_a_route()
    {
        Router::get('/test', function () {
            return 'no middleware';
        })->middleware('test');

        $response = $this->get('/test');

        $this->assertContains('test_middleware', (string) $response);
    }

    /** @test */
    public function multiple_middleware_can_bound_to_a_route()
    {
        Router::get('/test', function () {
            return 'no_middleware';
        })->middleware('another_test', 'test');

        $response = $this->get('/test', ['test' => 'yes']);

        $this->assertContains('another_middleware_test', (string) $response);

        $response = $this->get('/test');

        $this->assertContains('test_middleware', (string) $response);
    }

    // TODO: Tests for controller interaction + middleware.
}
