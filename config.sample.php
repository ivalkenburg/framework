<?php

return [
    'base'        => dirname(__FILE__),
    'debug'       => false,
    'maintenance' => false,
    'controllers' => '\\App\\Controllers\\',

    'database' => [
        'driver'    => 'mysql',
        'host'      => '127.0.0.1',
        'database'  => 'framework',
        'username'  => '',
        'password'  => '',
        'charset'   => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
    ],

    'mail' => [
        'host'       => 'smtp.mailtrap.io',
        'username'   => '',
        'password'   => '',
        'encryption' => 'tls',
        'port'       => 2525,
        'from'       => [
            'email' => 'john@cena.com',
            'name'  => 'John Cena',
        ],
    ],

    'templates' => [
        'location' => dirname(__FILE__).'/app/Resources/Views',
        'cache'    => false,
    ],
];
