<?php

namespace Wasf\Support;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;

class ParameterBag implements Countable, IteratorAggregate, ArrayAccess
{
    protected $parameters;

    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
    }

    public function all()
    {
        return $this->parameters;
    }

    public function keys()
    {
        return array_keys($this->parameters);
    }

    public function has($key)
    {
        return isset($this->parameters[$key]);
    }

    public function set($key, $value)
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    public function replace($parameters = [])
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function get($key, $default = null)
    {
        return isset($this->parameters[$key]) ? $this->parameters[$key] : $default;
    }

    public function add($parameters = [])
    {
        $this->parameters = array_replace($this->parameters, $parameters);
    }

    public function remove($key)
    {
        unset($this->parameters[$key]);
    }

    public function __get($key)
    {
        return $this->parameters[$key] ?? null;
    }

    public function intersect(...$arrays)
    {
        return array_intersect($this->parameters, ...$arrays);
    }

    public function map($callable)
    {
        foreach ($this->parameters as $key => $value) {
            $this->parameters[$key] = $callable($key, $value);
        }

        return $this;
    }

    public function filter($callable)
    {
        return array_filter($this->parameters, $callable, ARRAY_FILTER_USE_BOTH);
    }

    public function __isset($property)
    {
        return $this->has($property);
    }

    public function __unset($property)
    {
        $this->remove($property);
    }

    public function count()
    {
        return count($this->parameters);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->parameters);
    }

    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }
}
