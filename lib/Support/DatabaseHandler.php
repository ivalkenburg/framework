<?php

namespace Wasf\Support;

use Illuminate\Database\Capsule\Manager;

class DatabaseHandler
{
    protected $database;

    public function __construct()
    {
        $instance = new Manager();
        $instance->addConnection(config('database'));
        $instance->setAsGlobal();
        $instance->bootEloquent();

        $this->database = $instance->getConnection();
    }

    public function __call($method, $args)
    {
        return $this->database->$method(...$args);
    }
}
