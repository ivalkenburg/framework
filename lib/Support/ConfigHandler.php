<?php

namespace Wasf\Support;

use InvalidArgumentException;

class ConfigHandler
{
    protected $config;

    public function __construct()
    {
        $this->config = require __DIR__.'/../../config.php';
    }

    public function get($path = null, $default = null)
    {
        if (!is_null($path) && $path !== '') {
            $config = $this->config;
            $path = explode('/', trim($path, '/'));

            foreach ($path as $element) {
                if (isset($config[$element])) {
                    $config = $config[$element];
                } else {
                    if (!is_null($default)) {
                        return $default;
                    }

                    throw new InvalidArgumentException('No such ['.implode('/', $path).'] configuration set', 500);
                }
            }

            return $config;
        }

        return $this->config;
    }

    public function set($key, $value)
    {
        // TODO: add setting config at runtime.
    }
}
