<?php

use Wasf\Cookie\CookieFactory;
use Wasf\Core\ServiceContainer;
use Wasf\Routing\Request;
use Wasf\Routing\Response\RedirectFactory;
use Wasf\Routing\Response\ResponseFactory;
use Wasf\Support\ConfigHandler;

if (!function_exists('request')) {
    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app(Request::class);
        }

        return app(Request::class)->input($key, $default);
    }
}

if (!function_exists('app')) {
    function app($service = null, ...$arguments)
    {
        if (is_null($service)) {
            return ServiceContainer::init();
        }

        return ServiceContainer::init()->get($service, ...$arguments);
    }
}

if (!function_exists('config')) {
    function config($path = null, $default = null)
    {
        $config = app(ConfigHandler::class);

        if (is_array($path)) {
            return $config->set($path);
        }

        return $config->get($path, $default);
    }
}

if (!function_exists('response')) {
    function response($content = null, $status = null)
    {
        if (is_null($content)) {
            return new ResponseFactory();
        }

        return (new ResponseFactory())->make($content, $status);
    }
}

if (!function_exists('redirect')) {
    function redirect($url = null)
    {
        if (is_null($url)) {
            return new RedirectFactory();
        }

        return (new RedirectFactory())->to($url);
    }
}

if (!function_exists('back')) {
    function back($fallback = '/')
    {
        return (new RedirectFactory())->back($fallback);
    }
}

if (!function_exists('old')) {
    function old($key, $default = null)
    {
        return app(Request::class)->session->old($key, $default);
    }
}

if (!function_exists('view')) {
    function view($template, $data = [], $status = null)
    {
        return (new ResponseFactory())->view($template, $data, $status);
    }
}

if (!function_exists('json')) {
    function json($array, $status = null)
    {
        return (new ResponseFactory())->json($array, $status);
    }
}

if (!function_exists('cookie')) {
    function cookie($name = null, $value = null, $lifetime = 0, $path = null, $domain = null, $secure = false, $httpOnly = true)
    {
        $factory = new CookieFactory();

        if (is_null($name)) {
            return $factory;
        }

        return $factory->make($name, $value, $lifetime, $path, $domain, $secure, $httpOnly);
    }
}
