<?php

namespace Wasf\Support;

use Twig_Environment;
use Twig_Loader_Filesystem;
use Wasf\Routing\Response\ViewResponse;

class ViewHandler
{
    protected $shared = [];
    protected $twig;

    public function share($key, $value = null)
    {
        if (is_array($key)) {
            $this->shared = array_merge($this->shared, $key);
        } else {
            $this->shared[$key] = $value;
        }

        return $this;
    }

    public function build($template, $data)
    {
        return new ViewResponse($template, $data);
    }

    public function render($template, $data)
    {
        return $this->init()->render($template.'.twig', $this->compileData($data));
    }

    protected function compileData($data)
    {
        return array_merge($this->shared, $data);
    }

    protected function init()
    {
        if (!isset($this->twig)) {
            $config = config('templates');

            $loader = new Twig_Loader_Filesystem($config['location']);
            $this->twig = new Twig_Environment($loader, ['cache' => $config['cache']]);
        }

        return $this->twig;
    }
}
