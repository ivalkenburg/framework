<?php

namespace Wasf\Core;

use Exception;
use Wasf\Apps\DB;
use Wasf\Apps\Router;
use Wasf\Core\Exceptions\HTTPNotFoundException;
use Wasf\Routing\Pipeline;
use Wasf\Routing\Request;
use Wasf\Routing\Response\ExceptionResponse;
use Wasf\Routing\Response\Response;
use Wasf\Routing\Route;

class Kernel
{
    public function process(Request $request)
    {
        DB::init();

        if ($response = $this->handleRequest($request)) {
            if (php_sapi_name() === 'cli') {
                return $response;
            }

            $this->handleResponse($response);
        }
    }

    protected function handleRequest(Request $request)
    {
        try {
            if ($route = Router::match($request)) {
                $middleware = $this->compileMiddleware($route);
                $destination = $route->getCallable();

                return (new Pipeline($route))->with($request)->through($middleware)->then($destination);
            }

            throw new HTTPNotFoundException();
        } catch (Exception $exception) {
            return new ExceptionResponse($exception);
        }
    }

    protected function compileMiddleware(Route $route)
    {
        $stack = $this->middleware;
        $middleware = $route->getMiddleware();

        foreach ($middleware as $module) {
            $stack[] = $this->middlewareAliases[$module];
        }

        return $stack;
    }

    protected function handleResponse(Response $response)
    {
        if ($response->getStatus()) {
            http_response_code($response->getStatus());
        }

        foreach ($response->getHeaders() as $field => $value) {
            header("$field: $value");
        }

        echo $response;
    }
}
