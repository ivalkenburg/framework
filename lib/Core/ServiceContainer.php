<?php

namespace Wasf\Core;

use Wasf\Core\Exceptions\GeneralException;

class ServiceContainer
{
    protected static $instance;

    protected $aliases = [];
    protected $container = [];

    public function __construct()
    {
        $this->aliases = require __DIR__.'/aliases.php';
    }

    public static function init()
    {
        if (!isset(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function flush()
    {
        if (isset(static::$instance)) {
            static::$instance = null;
        }
    }

    public function has($service)
    {
        return isset($this->container[$service]) || isset($this->aliases[$service]);
    }

    public function get($service, ...$arguments)
    {
        if (isset($this->aliases[$service])) {
            $service = $this->aliases[$service];
        }

        if (isset($this->container[$service])) {
            return $this->container[$service];
        }

        if (class_exists($service)) {
            return $this->container[$service] = new $service(...$arguments);
        }

        throw new GeneralException("Unable to resolve [$service] service.");
    }

    public function forget($service)
    {
        if (isset($this->aliases[$service])) {
            $service = $this->aliases[$service];
        }

        if (isset($this->container[$service])) {
            unset($this->container[$service]);
        }

        return $this;
    }

    public function __get($property)
    {
        return $this->get($property);
    }
}
