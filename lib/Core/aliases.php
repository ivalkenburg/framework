<?php

return [
    'config'   => \Wasf\Support\ConfigHandler::class,
    'db'       => \Wasf\Support\DatabaseHandler::class,
    'router'   => \Wasf\Routing\RouteHandler::class,
    'kernel'   => \App\Kernel::class,
    'view'     => \Wasf\Support\ViewHandler::class,
    'request'  => \Wasf\Routing\Request::class,
    'resolver' => \Wasf\Core\Resolver::class,
];
