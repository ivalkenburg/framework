<?php

namespace Wasf\Core\Exceptions;

use Exception;

class HTTPNotFoundException extends Exception
{
    public function __construct($message = '404 Not Found', $code = 404, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
