<?php

namespace Wasf\Core\Exceptions;

use Exception;

class GeneralException extends Exception
{
    public function __construct($message = 'Internal Server Error', $code = 500, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
