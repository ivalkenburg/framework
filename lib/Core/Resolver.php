<?php

namespace Wasf\Core;

use BadMethodCallException;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use Wasf\Core\Exceptions\GeneralException;

class Resolver
{
    protected $container;

    public function __construct()
    {
        $this->container = ServiceContainer::init();
    }

    public function resolveClass($class, $arguments = [])
    {
        $reflection = new ReflectionClass($class);

        if (!$constructor = $reflection->getConstructor()) {
            return new $class();
        }

        return $reflection->newInstanceArgs($this->resolveParameters($constructor, $arguments));
    }

    public function resolveMethodCall($callable, $arguments = [])
    {
        if (!method_exists($callable[0], $callable[1])) {
            throw new BadMethodCallException('Method ['.get_class($callable[0]).'::'.$callable[1].'] does not exist', 500);
        }

        $reflection = new ReflectionMethod($callable[0], $callable[1]);

        return call_user_func_array($callable, $this->resolveParameters($reflection, $arguments));
    }

    public function resolveFunctionCall($callable, $arguments = [])
    {
        $reflection = new ReflectionFunction($callable);

        return call_user_func_array($callable, $this->resolveParameters($reflection, $arguments));
    }

    protected function resolveParameters($subject, $arguments)
    {
        $dependencies = [];

        foreach ($subject->getParameters() as $parameter) {
            if (isset($arguments[$parameter->name])) {
                $dependencies[] = $arguments[$parameter->name];
            } elseif ($dependency = $parameter->getClass()) {
                if ($this->container->has($dependency->name)) {
                    $dependencies[] = $this->container->get($dependency->name);
                } else {
                    $dependencies[] = $this->resolveClass($dependency->name);
                }
            } elseif ($parameter->isDefaultValueAvailable()) {
                $dependencies[] = $parameter->getDefaultValue();
            } else {
                throw new GeneralException("Failed to resolve [{$parameter->name}] parameter");
            }
        }

        return $dependencies;
    }
}
