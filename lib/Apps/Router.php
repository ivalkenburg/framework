<?php

namespace Wasf\Apps;

use Wasf\Routing\RouteHandler;

class Router extends BaseApp
{
    protected static function getAlias()
    {
        return RouteHandler::class;
    }
}
