<?php

namespace Wasf\Apps;

use Wasf\Support\DatabaseHandler;

class DB extends BaseApp
{
    protected static function getAlias()
    {
        return DatabaseHandler::class;
    }
}
