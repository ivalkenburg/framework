<?php

namespace Wasf\Apps;

class Resolver extends BaseApp
{
    protected static function getAlias()
    {
        return \Wasf\Core\Resolver::class;
    }
}
