<?php

namespace Wasf\Apps;

abstract class BaseApp
{
    public static function init()
    {
        return app(static::getAlias());
    }

    abstract protected static function getAlias();

    public static function __callStatic($method, $args)
    {
        return app(static::getAlias())->$method(...$args);
    }
}
