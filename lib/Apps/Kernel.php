<?php

namespace Wasf\Apps;

class Kernel extends BaseApp
{
    protected static function getAlias()
    {
        return \App\Kernel::class;
    }
}
