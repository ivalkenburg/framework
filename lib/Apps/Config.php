<?php

namespace Wasf\Apps;

use Wasf\Support\ConfigHandler;

class Config extends BaseApp
{
    protected static function getAlias()
    {
        return ConfigHandler::class;
    }
}
