<?php

namespace Wasf\Apps;

use Wasf\Routing\Response\RedirectFactory;

class Redirect extends BaseApp
{
    protected static function getAlias()
    {
        return RedirectFactory::class;
    }
}
