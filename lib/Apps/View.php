<?php

namespace Wasf\Apps;

use Wasf\Support\ViewHandler;

class View extends BaseApp
{
    protected static function getAlias()
    {
        return ViewHandler::class;
    }
}
