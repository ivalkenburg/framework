<?php

namespace Wasf\Apps;

use Wasf\Routing\Response\ResponseFactory;

class Response extends BaseApp
{
    protected static function getAlias()
    {
        return ResponseFactory::class;
    }
}
