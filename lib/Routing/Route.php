<?php

namespace Wasf\Routing;

use Wasf\Apps\Resolver;

class Route
{
    protected $path;
    protected $action;
    protected $name;
    protected $constraints = [];
    protected $middleware = [];
    protected $controller;

    public function __construct($path, $action)
    {
        $this->path = $path;
        $this->action = $action;
    }

    public function name($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function where($field, $regex = null)
    {
        if (is_array($field)) {
            $this->constraints = $field;
        } elseif (!is_array($field) && !is_null($regex)) {
            $this->constraints[$field] = $regex;
        }

        return $this;
    }

    public function getRegex()
    {
        $regex = str_replace('/', '\\/', $this->path);

        foreach ($this->constraints as $field => $constraint) {
            $regex = preg_replace('/{('.$field.')}/', '(?<\1>'.$constraint.')', $regex);
        }

        return '/^'.preg_replace('/{([a-z]+)}/', '(?<\1>.+)', $regex).'$/i';
    }

    public function isCallback()
    {
        return is_callable($this->action);
    }

    protected function getController()
    {
        if (!$this->controller) {
            $this->controller = Resolver::resolveClass($this->getCallable()[0]);
        }

        return $this->controller;
    }

    public function getCallable()
    {
        if ($this->isCallback()) {
            return $this->action;
        }

        list($controller, $method) = explode('@', $this->action, 2);

        if ($this->controller) {
            return [$this->controller, $method];
        }

        return [config('controllers').$controller, $method];
    }

    public function middleware($middleware)
    {
        $this->middleware = is_array($middleware) ? $middleware : func_get_args();

        return $this;
    }

    public function getMiddleware()
    {
        if ($this->isCallback()) {
            return $this->middleware;
        }

        return array_merge($this->middleware, $this->getController()->getMiddleware($this->getCallable()[1]));
    }
}
