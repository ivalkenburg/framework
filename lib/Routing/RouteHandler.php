<?php

namespace Wasf\Routing;

class RouteHandler
{
    protected $routes;

    public function __construct()
    {
        $this->routes = new RouteCollection();
    }

    public function get($path, $action)
    {
        return $this->routes->add('GET', $path, $action);
    }

    public function post($path, $action)
    {
        return $this->routes->add('POST', $path, $action);
    }

    public function getRoutes()
    {
        return $this->routes;
    }

    public function match(Request $request)
    {
        return $this->routes->match($request);
    }

    public function flush()
    {
        $this->routes->flush();

        return $this;
    }
}
