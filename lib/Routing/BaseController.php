<?php

namespace Wasf\Routing;

abstract class BaseController
{
    protected $middleware = [];

    public function getMiddleware($method)
    {
        if (empty($this->middleware)) {
            return [];
        }

        return array_column(array_filter($this->middleware, function ($value) use ($method) {
            return (isset($value['options']['only']) && in_array($method, $value['options']['only'])) ||
                   (isset($value['options']['except']) && !in_array($method, $value['options']['except'])) ||
                   (empty($value['options']));
        }), 'middleware');
    }

    protected function middleware($middleware)
    {
        $options = [];

        foreach ((array) $middleware as $m) {
            $this->middleware[] = ['middleware' => $m, 'options' => &$options];
        }

        return new ControllerMiddlewareOptions($options);
    }
}
