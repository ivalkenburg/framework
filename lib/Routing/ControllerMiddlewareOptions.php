<?php

namespace Wasf\Routing;

class ControllerMiddlewareOptions
{
    protected $options;

    public function __construct(&$options)
    {
        $this->options = &$options;
    }

    public function only($method)
    {
        $this->options['only'] = is_array($method) ? $method : func_get_args();

        return $this;
    }

    public function except($method)
    {
        $this->options['except'] = is_array($method) ? $method : func_get_args();

        return $this;
    }
}
