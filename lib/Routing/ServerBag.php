<?php

namespace Wasf\Routing;

use Wasf\Support\ParameterBag;

class ServerBag extends ParameterBag
{
    public function getHeaders()
    {
        $headers = [];

        foreach ($this->parameters as $key => $value) {
            if (strpos($key, 'HTTP_') === 0) {
                $headers[$this->formatHeader(substr($key, 5))] = $value;
            }
        }

        return $headers;
    }

    protected function formatHeader($header)
    {
        return implode('-', array_map('ucfirst', explode('_', strtolower($header))));
    }
}
