<?php

namespace Wasf\Routing\Middleware;

use Wasf\Routing\Request;

class CheckMaintenance extends BaseMiddleware
{
    public function process(Request $request)
    {
        // TODO: Proper maintenance response

        if (config('maintenance')) {
            return response('Site is in maintenance mode.');
        }
    }
}
