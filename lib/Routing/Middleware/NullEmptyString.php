<?php

namespace Wasf\Routing\Middleware;

use Wasf\Routing\Request;

class NullEmptyString extends BaseMiddleware
{
    protected function process(Request $request)
    {
        $request->get()->map([$this, 'nullString']);
        $request->post()->map([$this, 'nullString']);
    }

    public function nullString($key, $value)
    {
        if ($value === '') {
            return;
        }

        return $value;
    }
}
