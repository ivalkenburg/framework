<?php

namespace Wasf\Routing\Middleware;

use Wasf\Routing\Request;

class TrimInput extends BaseMiddleware
{
    protected function process(Request $request)
    {
        $request->get()->map([$this, 'trimString']);
        $request->post()->map([$this, 'trimString']);
    }

    public function trimString($key, $value)
    {
        if (in_array($key, $this->except) && is_string($value)) {
            return $value;
        }

        return trim($value);
    }
}
