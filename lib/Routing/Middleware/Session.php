<?php

namespace Wasf\Routing\Middleware;

use Wasf\Routing\Request;

class Session extends BaseMiddleware
{
    public function process(Request $request)
    {
        if (php_sapi_name() !== 'cli') {
            session_start();
        }
    }
}
