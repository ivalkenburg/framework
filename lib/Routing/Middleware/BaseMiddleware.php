<?php

namespace Wasf\Routing\Middleware;

use Wasf\Routing\Request;

abstract class BaseMiddleware
{
    protected $successor;

    public function setSuccessor(self $successor)
    {
        if (!isset($this->successor)) {
            $this->successor = $successor;
        } else {
            $this->successor->setSuccessor($successor);
        }

        return $this;
    }

    public function handle(Request $request)
    {
        $response = $this->process($request);

        if ($response === null && isset($this->successor)) {
            $response = $this->successor->handle($request);
        }

        return $response;
    }

    abstract protected function process(Request $request);
}
