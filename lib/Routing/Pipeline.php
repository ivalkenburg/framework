<?php

namespace Wasf\Routing;

use HttpResponseException;
use Wasf\Apps\Resolver;
use Wasf\Routing\Response\Response;
use Wasf\Routing\Response\ResponseFactory;

class Pipeline
{
    protected $request;
    protected $middleware;
    protected $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    public function with(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    public function through($middleware)
    {
        $this->middleware = $middleware;

        return $this;
    }

    public function then($destination)
    {
        $first = array_shift($this->middleware);
        $chain = Resolver::resolveClass($first);

        foreach ($this->middleware as $module) {
            $chain->setSuccessor(Resolver::resolveClass($module));
        }

        if ($response = $chain->handle($this->request)) {
            return $this->handleResponse($response);
        }

        return $this->handleResponse($this->getResponse($destination));
    }

    protected function handleResponse($response)
    {
        if ($response instanceof Response) {
            return $response;
        } elseif (method_exists($response, '__toString')) {
            return (new ResponseFactory())->make((string) $response);
        } elseif (is_string($response)) {
            return (new ResponseFactory())->make($response);
        } elseif (is_array($response)) {
            return (new ResponseFactory())->json($response);
        } else {
            throw new HttpResponseException('No valid response supplied');
        }
    }

    protected function getResponse($destination)
    {
        if ($this->route->isCallback()) {
            return Resolver::resolveFunctionCall($destination, $this->request->arguments());
        } else {
            return Resolver::resolveMethodCall($destination, $this->request->arguments());
        }
    }
}
