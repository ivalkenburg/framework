<?php

namespace Wasf\Routing;

class RouteCollection
{
    protected $routes = [];

    public function add($method, $path, $action)
    {
        $path = strpos($path, '/') === 0 ? $path : '/'.$path;
        $method = strtoupper($method);

        return $this->routes[$method][] = new Route($path, $action);
    }

    public function has($name, $method = 'GET')
    {
        $routes = $this->getRoutes($method);

        foreach ($routes as $route) {
            if ($name === $route->getName()) {
                return $route;
            }
        }
    }

    public function match(Request $request)
    {
        $path = $request->path();

        foreach ($this->getRoutes($request->method()) as $route) {
            if (preg_match($route->getRegex(), $path, $match)) {
                $arguments = [];
                foreach ($match as $key => $value) {
                    if (is_string($key)) {
                        $arguments[$key] = $value;
                    }
                }

                $request->bindRoute($route, $arguments);

                return $route;
            }
        }

        return false;
    }

    protected function getRoutes($method)
    {
        return $this->routes[strtoupper($method)] ?? [];
    }

    public function flush()
    {
        $this->routes = [];

        return $this;
    }
}
