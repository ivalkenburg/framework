<?php

namespace Wasf\Routing;

use Wasf\Support\ParameterBag;

class Request
{
    protected $post;
    protected $get;
    protected $cookies;
    protected $headers;
    protected $arguments;
    protected $route;
    protected $server;
    protected $session;

    public function __construct($server, $get, $post, $cookies)
    {
        $this->server = new ServerBag($server);
        $this->post = new ParameterBag($post);
        $this->get = new ParameterBag($get);
        $this->cookies = new ParameterBag($cookies);
        $this->headers = new ParameterBag($this->server->getHeaders());
    }

    public function all()
    {
        $isGet = $this->isMethod('GET');

        return array_merge(
            $isGet ? $this->post->all() : $this->get->all(),
            $isGet ? $this->get->all() : $this->post->all()
        );
    }

    public function input($key, $default = null)
    {
        return $this->method() === 'GET' ? $this->get->get($key, $default) : $this->post->get($key, $default);
    }

    public function isMethod($method)
    {
        return $this->method() === strtoupper($method);
    }

    public function method()
    {
        return strtoupper($this->server->get('REQUEST_METHOD'));
    }

    public function isAjax()
    {
        return $this->headers->get('X-Requested-With') === 'XMLHttpRequest';
    }

    public function wantsJson()
    {
        return $this->isAjax() || (strpos($this->headers->get('Accept'), 'application/json') !== false);
    }

    public function path()
    {
        $path = rtrim(parse_url($this->server->get('REQUEST_URI'), PHP_URL_PATH), '/');

        return $path === '' ? '/' : $path;
    }

    public function bindRoute(Route $route, $arguments = [])
    {
        $this->route = $route;
        $this->arguments = new ParameterBag($arguments);

        return $this;
    }

    public function route()
    {
        return $this->route;
    }

    public function post($key = null, $default = null)
    {
        return is_null($key) ? $this->post : $this->post->get($key, $default);
    }

    public function get($key = null, $default = null)
    {
        return is_null($key) ? $this->get : $this->get->get($key, $default);
    }

    public function arguments($key = null, $default = null)
    {
        return is_null($key) ? $this->arguments : $this->arguments->get($key, $default);
    }

    public function headers($key = null, $default = null)
    {
        return is_null($key) ? $this->headers : $this->headers->get($key, $default);
    }

    public function __get($property)
    {
        return $this->method() === 'GET' ? $this->get->$property : $this->post->$property;
    }
}
