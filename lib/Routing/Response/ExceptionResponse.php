<?php

namespace Wasf\Routing\Response;

use Exception;

class ExceptionResponse extends Response
{
    public function __construct(Exception $exception)
    {
        parent::__construct($exception->getMessage(), $exception->getCode());
    }
}
