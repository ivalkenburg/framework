<?php

namespace Wasf\Routing\Response;

class ResponseFactory
{
    public function make($content, $status = null, $headers = [])
    {
        if (is_array($content)) {
            return $this->json($content, $status, $headers);
        }

        return new Response($content, $status, $headers);
    }

    public function view($template, $data = [], $status = null, $headers = [])
    {
        return new ViewResponse($template, $data, $status, $headers);
    }

    public function json($content, $status = null, $headers = [])
    {
        return new JsonResponse($content, $status, $headers);
    }
}
