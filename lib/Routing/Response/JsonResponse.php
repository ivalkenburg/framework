<?php

namespace Wasf\Routing\Response;

class JsonResponse extends Response
{
    public function __construct($content = [], $status = null, $headers = [])
    {
        parent::__construct($content, $status, array_merge($headers, ['Content-Type' => 'application/json']));
    }

    public function __toString()
    {
        return json_encode($this->content);
    }
}
