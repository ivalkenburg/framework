<?php

namespace Wasf\Routing\Response;

use Wasf\Support\ViewHandler;

class ViewResponse extends Response
{
    protected $template;
    protected $data;

    public function __construct($template, $data, $status = null, $headers = [])
    {
        $this->template = $template;
        $this->data = $data;

        parent::__construct(null, $status, $headers);
    }

    public function with($key, $value = null)
    {
        if (is_array($key)) {
            $this->data = array_merge($this->data, $key);
        } else {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function __toString()
    {
        return (string) app(ViewHandler::class)->render($this->template, $this->data);
    }
}
