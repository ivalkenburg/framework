<?php

namespace Wasf\Routing\Response;

class RedirectResponse extends Response
{
    public function __construct($url, $status = 302)
    {
        parent::__construct($this->buildHTML($url), $status, ['Location' => $url]);
    }

    protected function buildHTML($url)
    {
        return sprintf('<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="1;url=%1$s">
    <title>Redirecting to %1$s</title>
</head>
<body>
    Redirecting to <a href="%1$s">%1$s</a>
</body>
</html>', htmlspecialchars($url, ENT_QUOTES, 'UTF-8'));
    }
}
