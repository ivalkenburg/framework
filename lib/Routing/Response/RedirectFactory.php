<?php

namespace Wasf\Routing\Response;

use InvalidArgumentException;
use Wasf\Routing\Request;
use Wasf\Routing\RouteHandler;

class RedirectFactory
{
    public function back($fallback = '/')
    {
        return new RedirectResponse(app(Request::class)->headers('Referer', $fallback));
    }

    public function to($url)
    {
        return new RedirectResponse($url);
    }

    public function route($name, $arguments = [])
    {
        $url = app(RouteHandler::class)->getRoutes()->has($name)->getPath();

        foreach (array_keys($arguments) as $key => $value) {
            $url = str_replace('{'.$key.'}', $value, $url);
        }

        if (preg_match('/{[a-z]+}/', $url) !== 0) {
            throw new InvalidArgumentException("Missing arguments for route [$name] redirect");
        }

        return new RedirectResponse($url);
    }
}
