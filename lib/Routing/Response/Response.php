<?php

namespace Wasf\Routing\Response;

use Wasf\Support\ParameterBag;

class Response
{
    protected $content;
    protected $headers;
    protected $status;

    public function __construct($content = null, $status = null, $headers = [])
    {
        $this->headers = new ParameterBag($headers);
        $this->content = $content;
        $this->status = $status;
    }

    public function header($field, $value)
    {
        $this->headers->set($field, $value);

        return $this;
    }

    public function withHeaders($headers)
    {
        $this->headers->add($headers);

        return $this;
    }

    public function status($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function __toString()
    {
        if (is_array($this->content)) {
            return json_encode($this->content);
        }

        return (string) $this->content;
    }

    public function cookie()
    {
        // TODO: create cookie object in cookiebag or cookiejar, read it when handling response and set cookies before outputting content.

        return $this;
    }

    public function flash()
    {
        // TODO: set flash data in session. when handling response we clear the previous flash data if applicable. we also need to extend twig with a function 'flash()' to read it out.

        return $this;
    }
}
